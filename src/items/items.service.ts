import { Injectable } from '@nestjs/common';
import { Item } from './interfaces/item.interface';

@Injectable()
export class ItemsService {
  private readonly items: Item[] = [
    {
      id: '32423',
      name: 'Item 1',
      qty: 2,
    },
    {
      id: '32424',
      name: 'Item 2',
      qty: 10,
    },
    {
      id: '32425',
      name: 'Item 3',
      qty: 0,
    },
  ];
}
