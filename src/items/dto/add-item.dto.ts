export class AddItemDto {
  readonly name: string;
  readonly desc: string;
  readonly qty: number;
}
