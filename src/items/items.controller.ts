import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { AddItemDto } from './dto/add-item.dto';

@Controller('items')
export class ItemsController {
  @Get()
  fetchAll(): object[] {
    return [{ msg: 'All items' }];
  }

  @Get(':id')
  fetchOne(@Param('id') id): object[] {
    return [{ item: id }];
  }

  @Post()
  addItem(@Body() addItemDto: AddItemDto): object[] {
    return [
      { name: addItemDto.name, desc: addItemDto.desc, qty: addItemDto.qty },
    ];
  }

  @Put(':id')
  updateItem(@Body() updateItemDto: AddItemDto, @Param('id') id): object[] {
    return [
      {
        id: id,
        name: updateItemDto.name,
        desc: updateItemDto.desc,
        qty: updateItemDto.qty,
      },
    ];
  }

  @Delete(':id')
  deleteItem(@Param('id') id): object[] {
    return [{ item: id }];
  }
}
